# skbuild_fortran_test

A sample project build with [scikit-build](https://scikit-build.readthedocs.io/en/latest/).

We provide a fortran wrapper file, that is linked against an external
[fortran library](https://git.ufz.de/muellese/fortran-lib-test) that provides
global variables and interfaces to manipulated them.
In addition, the external library is also linking against [FORCES](https://git.ufz.de/chs/forces/).

This fortran library will eventually be [mHM](https://git.ufz.de/mhm/mhm) in the context of FINAM.

The wrapper (`wrap_lib/wrapper.f90`) is just a small layer on top of these
interfaces to be compatible with [f2py](https://numpy.org/doc/stable/f2py/index.html).

To compile everything locally
([editable install](https://pip.pypa.io/en/stable/cli/pip_install/#install-editable)),
you can use pip:

```bash
pip install -e . --user
```

Afterwards you can call the wrapper routines from (I)Python:

```python
In [1]: import wrap_lib as wl

In [2]: wl.set_globals(1, 2., [3,4])

In [3]: wl.set_container(10, 20., [30,40])

In [4]: wl.print_vars()
 global int:           1
 global real:   2.0000000000000000
 global array:   3.0000000000000000        4.0000000000000000

 container int:          10
 container real:   20.000000000000000
 container array:   30.000000000000000        40.000000000000000
```
