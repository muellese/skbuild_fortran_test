import sys
import site
from skbuild import setup


# pip issue: https://github.com/pypa/pip/issues/7953
site.ENABLE_USER_SITE = "--user" in sys.argv[1:]

setup(
    name="wrap_lib",
    version="0.1",
    description="a minimal example package (fortran version)",
    author="Sebastian Mueller",
    license="MIT",
    packages=["wrap_lib"],
    install_requires=["numpy"],
    cmake_args=["-DCMAKE_BUILD_TYPE=Release"],
)
