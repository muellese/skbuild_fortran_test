subroutine set_globals(n,A,B,C)
  USE mo_lib_functions, ONLY: set_global_vars
  implicit none

  integer(4), intent(in)            :: n
  integer(4), intent(in)            :: A
  real(8), intent(in)               :: B
  real(8), dimension(n), intent(in) :: C

  call set_global_vars(A,B,C)

end subroutine set_globals

subroutine set_container(n,A,B,C)
  USE mo_lib_functions, ONLY: set_container_vars
  implicit none

  integer(4), intent(in)            :: n
  integer(4), intent(in)            :: A
  real(8), intent(in)               :: B
  real(8), dimension(n), intent(in) :: C

  call set_container_vars(A,B,C)

end subroutine set_container

subroutine print_vars()
  USE mo_lib_functions, ONLY: print_globals
  implicit none

  call print_globals()

end subroutine print_vars

subroutine print_bar()
  implicit none

  print*, "bar"

end subroutine print_bar

subroutine read_nc(fname, vname_data)
  USE mo_lib_functions, ONLY: readfile_alt
  implicit none

  character(len = *), intent(in) :: fname
  character(len = *), intent(in) :: vname_data

  call readfile_alt(fname, vname_data)

end subroutine read_nc
